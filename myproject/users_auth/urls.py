from django.urls import path, reverse_lazy, include
from users_auth import views
from django.contrib.auth.views import (
    LoginView,
    LogoutView,
    PasswordChangeView,
    PasswordChangeDoneView,
    PasswordResetView,
    PasswordResetDoneView,
    PasswordResetConfirmView,
    PasswordResetCompleteView
)


app_name = "users_auth"
urlpatterns = [
    path("dashboard/", views.dashboard, name="dashboard"),
    path(
        "accounts/login/",
        LoginView.as_view(template_name="reg/login.html"),
        name="login",
    ),
    path(
        "accounts/logout/",
        LogoutView.as_view(template_name="logged_out.html"),
        name="logout",
    ),
    path(
        "accounts/password_change/",
        PasswordChangeView.as_view(
            template_name="reg/password_change_form.html",
            success_url=reverse_lazy("users_auth:password_change_done"),
        ),
        name="password_change",
    ),
    path(
        "accounts/password_change/done/",
        PasswordChangeDoneView.as_view(template_name="reg/password_change_done.html"),
        name="password_change_done",
    ),
    path("accouns/password_reset/", PasswordResetView.as_view(
        template_name='reg/password_reset_form.html',
        success_url = reverse_lazy("users_auth:password_reset_done"),
        email_template_name = "reg/password_reset_email.html"
        ), name="password_reset"),
    path(
        "account/password_reset/done/",
        PasswordResetDoneView.as_view(template_name='reg/password_reset_done.html'),
        name="password_reset_done",
    ),
    path(
        "reset/<uidb64>/<token>/", PasswordResetConfirmView.as_view(
            success_url=reverse_lazy("users_auth:password_reset_complete"),
            template_name="reg/password_reset_confirm.html"
        ),
        name="password_reset_confirm",
    ),
    path(
        "reset/done/",
        PasswordResetCompleteView.as_view(
            template_name="reg/password_reset_complete.html"
        ),
        name="password_reset_complete",
    ),
    path("register/", views.register, name="register"),
    path("oauth/", include("social_django.urls")),
]
